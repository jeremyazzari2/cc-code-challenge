require 'rails_helper'

describe FinancialSummary do
  it 'factory methods return an instance' do
    user = create(:user)

    expect(FinancialSummary.one_day(user: user).is_a?(FinancialSummary)).to eq(true)
    expect(FinancialSummary.seven_days(user: user).is_a?(FinancialSummary)).to eq(true)
    expect(FinancialSummary.lifetime(user: user).is_a?(FinancialSummary)).to eq(true)
  end

  it 'summarizes over one day' do
    user = create(:user)

    Timecop.freeze(Time.now) do
      create(:transaction, user: user, category: :deposit, amount: Money.from_amount(2.12, :usd))
      create(:transaction, user: user, category: :deposit, amount: Money.from_amount(10, :usd))
    end

    Timecop.freeze(2.days.ago) do
      create(:transaction, user: user, category: :deposit)
    end

    Timecop.freeze(1.day.ago - 1.second) do
      create(:transaction, user: user, category: :deposit)
    end

    subject = FinancialSummary.one_day(user: user, currency: :usd)
    expect(subject.count(:deposit)).to eq(2)
    expect(subject.amount(:deposit)).to eq(Money.from_amount(12.12, :usd))
  end

  it 'summarizes over seven days' do
    user = create(:user)

    Timecop.freeze(5.days.ago) do
      create(:transaction, user: user, category: :deposit, amount: Money.from_amount(2.12, :usd))
      create(:transaction, user: user, category: :deposit, amount: Money.from_amount(10, :usd))
    end

    Timecop.freeze(8.days.ago) do
      create(:transaction, user: user, category: :deposit)
    end

    Timecop.freeze(7.days.ago - 1.second) do
      create(:transaction, user: user, category: :deposit)
    end

    subject = FinancialSummary.seven_days(user: user, currency: :usd)
    expect(subject.count(:deposit)).to eq(2)
    expect(subject.amount(:deposit)).to eq(Money.from_amount(12.12, :usd))
  end

  it 'summarizes over lifetime' do
    user = create(:user)

    Timecop.freeze(30.days.ago) do
      create(:transaction, user: user, category: :deposit, amount: Money.from_amount(2.12, :usd))
      create(:transaction, user: user, category: :deposit, amount: Money.from_amount(10, :usd))
    end

    Timecop.freeze(8.days.ago) do
      create(:transaction, user: user, category: :deposit)
    end

    subject = FinancialSummary.lifetime(user: user, currency: :usd)
    expect(subject.count(:deposit)).to eq(3)
    expect(subject.amount(:deposit)).to eq(Money.from_amount(13.12, :usd))
  end

  it 'finds no transactions for a user' do
    user = create(:user)

    subject = FinancialSummary.one_day(user: user, currency: :usd)
    expect(subject.count(:deposit)).to eq(0)
    expect(subject.amount(:deposit)).to eq(Money.from_amount(0, :usd))

    subject = FinancialSummary.seven_days(user: user, currency: :usd)
    expect(subject.count(:deposit)).to eq(0)
    expect(subject.amount(:deposit)).to eq(Money.from_amount(0, :usd))

    subject = FinancialSummary.lifetime(user: user, currency: :usd)
    expect(subject.count(:deposit)).to eq(0)
    expect(subject.amount(:deposit)).to eq(Money.from_amount(0, :usd))
  end

  it 'defaults currency to :usd' do
    user = create(:user)
    create(:transaction, user: user, category: :deposit, amount: Money.from_amount(2.12, :usd))

    subject = FinancialSummary.one_day(user: user)
    expect(subject.amount(:deposit)).to eq(Money.from_amount(2.12, :usd))

    subject = FinancialSummary.seven_days(user: user)
    expect(subject.amount(:deposit)).to eq(Money.from_amount(2.12, :usd))

    subject = FinancialSummary.lifetime(user: user)
    expect(subject.amount(:deposit)).to eq(Money.from_amount(2.12, :usd))
  end

  describe '#count' do
    it 'only returns results from the given category' do
      user = create(:user)
      create(:transaction, user: user, category: :withdraw, amount: Money.from_amount(2.12, :usd))
      create(:transaction, user: user, category: :deposit, amount: Money.from_amount(3.12, :usd))
      create(:transaction, user: user, category: :deposit, amount: Money.from_amount(4.12, :usd))

      subject = FinancialSummary.new(user: user, currency: :usd)
      expect(subject.count(:withdraw)).to eq(1)
      expect(subject.count(:deposit)).to eq(2)
    end

    it 'requires a category' do
      user = create(:user)
      create(:transaction, user: user, category: :withdraw, amount: Money.from_amount(2.12, :usd))

      subject = FinancialSummary.new(user: user, currency: :usd)
      expect { subject.count }.to raise_error(ArgumentError)
    end
  end

  describe '#amount' do
    it 'only returns results from the given category' do
      user = create(:user)
      create(:transaction, user: user, category: :withdraw, amount: Money.from_amount(2.12, :usd))
      create(:transaction, user: user, category: :deposit, amount: Money.from_amount(3.12, :usd))
      create(:transaction, user: user, category: :deposit, amount: Money.from_amount(4.12, :usd))

      subject = FinancialSummary.new(user: user, currency: :usd)
      expect(subject.amount(:withdraw)).to eq(Money.from_amount(2.12, :usd))
      expect(subject.amount(:deposit)).to eq(Money.from_amount(7.24, :usd))
    end

    it 'requires a category' do
      user = create(:user)
      create(:transaction, user: user, category: :withdraw, amount: Money.from_amount(2.12, :usd))

      subject = FinancialSummary.new(user: user, currency: :usd)
      expect { subject.amount }.to raise_error(ArgumentError)
    end

    it 'uses the currency attribute in result' do
      user = create(:user)
      create(:transaction, user: user, category: :deposit, amount: Money.from_amount(2.12, :cad))

      subject = FinancialSummary.new(user: user, currency: :cad)
      expect(subject.amount(:deposit)).to eq(Money.from_amount(2.12, :cad))
    end
  end
end
