class FinancialSummary
  def initialize(options = {})
    @user = options[:user]
    @currency = options[:currency] || :usd
    @created_at_range = options[:created_at_range]
  end

  def count(category)
    transactions.where(category: category).calculate(:count, :all)
  end

  def amount(category)
    amount_cents = transactions.where(category: category).sum(:amount_cents)
    Money.new(amount_cents, @currency)
  end

  class << self
    def one_day(user:, currency: nil)
      new(
        user: user,
        currency: currency,
        created_at_range: {
          start_time: 1.day.ago,
          end_time: Time.now
        }
      )
    end

    def seven_days(user:, currency: nil)
      new(
        user: user,
        currency: currency,
        created_at_range: {
          start_time: 7.days.ago,
          end_time: Time.now
        }
      )
    end

    def lifetime(user:, currency: nil)
      new(user: user, currency: currency)
    end
  end

  private

  def transactions
    where_clause = created_at_range_clause || {}
    @transactions ||= @user.transactions.where(where_clause)
  end

  def created_at_range_clause
    { created_at: @created_at_range[:start_time]..@created_at_range[:end_time] } if @created_at_range
  end
end
